<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper" id="wrapper-footer">

	<div class="container-1600">

		<div class="row">

			<div class="col-md-12">

				<footer class="site-footer" id="colophon">

					<div class="site-info">
					<!-- Copyright -->
						<div class="row footer-row">
							<div class="col-lg-5 footer-cols">
								
							 <?php if( get_field('copyright_text', 'option')) : ?>
									<p><?php the_field('copyright_text', 'option') ?></p>
							  <?php endif; ?>
							  
						   </div>
						   <!-- Copyright -->
						   <div class="col-lg-7 footer-cols">
							   <div class="row">
								   <div class="col-lg-4">
								   <?php if( get_field('email', 'option')) : ?>
									<a href="mailto:<?php the_field('email', 'option') ?>"><?php the_field('email', 'option') ?></a>
								   <?php endif; ?>

								   </div>
								   <div class="col-lg-4">
								   <?php if( get_field('tel', 'option')) : ?>
									<a href="tel:<?php the_field('tel', 'option') ?>"><?php the_field('tel', 'option') ?></a>
								   <?php endif; ?>

								   </div>
								   <div class="col-lg-4">
									   <div class="socials">
									   <?php if(have_rows('social_links','option')): ?>
									   <p>#<?php the_field('instagram', 'option') ?></p>
									   	<?php while(have_rows('social_links','option')): the_row(); ?>
											<a href="<?php the_sub_field('url', 'option'); ?>"><i class="fa fa-<?php the_sub_field('icon','option'); ?>"></i></a>	
										<?php endwhile; ?>
									   <?php endif; ?>
									   </div>
									   
								   </div>
							   </div>
						   </div>
						</div>
					<!-- End Copyright -->
					
					      </div>
						
					</div><!-- .site-info -->

				</footer><!-- #colophon -->

			</div><!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>

