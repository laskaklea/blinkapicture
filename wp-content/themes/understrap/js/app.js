jQuery(document).ready(function($) {

    $(window).scroll(function() {
        if ($(window).width() >= 600) {

            if ($(window).scrollTop() >= 300) {
                $('nav').addClass('scrolled');
            } else {
                $('nav').removeClass('scrolled');
            }

        }
    });

    $('.navbar-toggler').on('click',function(){
        if($(this).hasClass('hidden')){
            $(this).removeClass('hidden');
            $(this).addClass('show-menu');

        }
        else{
            $(this).removeClass('show-menu');
            $(this).addClass('hidden');
        }
    })

    $('.owl-carousel').owlCarousel({
        margin:20,
        nav: true,
        loop: true,
        dots: true,
        responsive: { 
                0:{
                    items: 1,
                    dots: true,
                    nav: false,

                },
                800:{
                    items: 3,
                },
                1000:{
                    items: 4,
                }
            }
        });
    
    // open mobile menu
    $('.navbar-toggle').click(function(e){
      e.preventDefault();
      $('.mobile-header-nav').slideToggle();
      $(this).toggleClass('open');
    });

});
