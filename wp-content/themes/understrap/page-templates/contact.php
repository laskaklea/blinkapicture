<?php
/**
 * Template Name: Contact
 *
 * Template for displaying about me page
 * 
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );//?

?>

<div class="wrapper" id="page-wrapper">

    <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
        <!--?-->

        <div class="row">

            <div class="col-12 ">
                <section class="section contact-form-section">
                    <?php $formId=get_field('form_id'); ?>
                    <?php $formTitle=get_field('form_title'); ?>
                    <?php echo do_shortcode( '[contact-form-7 id="'.$formId.'" title="'.$formTitle. '"]');?>
                </section>
            </div>

        </div>
        <!-- .row -->

    </div>
    <!-- #content -->

</div>
<!-- #page-wrapper -->

<?php get_footer(); ?>