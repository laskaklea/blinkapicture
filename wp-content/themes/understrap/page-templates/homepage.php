<?php
/**
 * Template Name: Homepage
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header(); 
$bannerImage = get_field('banner_image');
?>
<div class="wrapper maincontent" id="full-width-page-wrapper">
	
    <!-- Start Main Banner Section -->
    <section class="section main-banner-section" style="background-image: url('<?php echo $bannerImage['url']; ?>');">
        <div class="container-fluid">
            <div class="col-12">
            </div>
        </div>
    </section>
    <!-- End Main Banner Section -->
    <?php if( have_rows('categories') ) : ?>
    <!-- Start Categories Section -->
        <section class="section categories-section">
            <div class="container-fluid">
                <div class="row">
                <?php while (have_rows('categories')) : the_row(); ?>
                    <?php $image = get_sub_field('image') ?>
                     <div class="col-lg-15">
                        <a href="<?php the_sub_field('url'); ?>">
                            <div class="image-shadow">
                                <img class="img-responsive" src="<?php echo $image['url']; ?>" alt="">
                            </div>
                       </a>
                     </div>
                <?php endwhile;?>
                </div>
            </div>
        </section>
    <!-- End Categories Section -->
    <?php endif; ?>

    <?php if( get_field('quote_text')) : ?>
    <!-- Start Quote Section -->
        <section class="section quote-section">
            <div class="container">
                <div class="col-lg-12">
                    <p><?php the_field('quote_text') ?></p>
                </div>
            </div>

        </section>
    <!-- End Quote Section -->
    <?php endif; ?>


    <?php // WP_Query arguments
    $args = array (
        'post_type'              => array( 'testimonials' ),
        'post_status'            => array( 'publish' ),
        'nopaging'               => true,
        'order'                  => 'ASC',
        'orderby'                => 'menu_order',
        'posts_per_page'         => -1,
    );

    // The Query
    $testimonials = new WP_Query( $args );

    // The Loop
    if ( $testimonials->have_posts() ) { ?>
    <!-- Start Testimonials Section -->
    <section class="section testimonials-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title">
                        <h1 class="title-h1 title-calcio">
                            Testimonials
                        </h1>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row owl-carousel owl-theme">
                    <?php while ( $testimonials->have_posts() ) {
                        $testimonials->the_post(); ?>
                        <div class="testimonial-holder item">
                            <div class="upper-holder">    
                                <p class="description">
                                    <?php 
                                    $testimonial = get_post();
                                    echo $testimonial->post_excerpt; ?>
                                </p>
                            </div>
                            <div class="bottom-holder">
                                <p class="testimonial-title"><b><?php the_title(); ?></b></p>
                                <p class="reviewer"><?php the_field('reviewer_name') ?> review on <?php the_field('date'); ?></p>
                            </div>
                        </div>
                    <?php } ?>
                    </div>
                </div>

            <?php } ?>
            </div>    
        </div>
    </section>
    <!-- End Testimonials Section -->
    <?php
    // Restore original Post Data
    wp_reset_postdata();
    ?>

    <!-- Start Instagram Section -->
    <section class="section instagram-feed-section">
        <div class="col-lg-12">
                    <div class="title">
                        <h1 class="title-h1 title-calcio">
                            New on Instagram
                        </h1>
                    </div>
                </div>
            <?php $shortcode_var = get_field('instagram_feed_shortcode'); ?>
            <?php echo do_shortcode($shortcode_var); ?>
    </section>
    <!-- End Instagram Section -->

    <!-- Start Contact Section -->
    <section class="section contact-me-section">
        <div class="col-lg-12">
            <div class="title">
                <h1 class="title-h1 title-leaf">
                    <?php the_field('section_title'); ?>
                </h1>
            </div>
            <section class="section contact-form-section">
                <?php $formId=get_field('form_id'); ?>
                <?php $formTitle=get_field('form_title'); ?>
                <?php echo do_shortcode( '[contact-form-7 id="'.$formId.'" title="'.$formTitle. '"]');?>
            </section>
        </div>
    </section>
    <!-- End Contact Section -->
</div>
<?php get_footer();?>