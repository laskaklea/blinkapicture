<?php
/**
 * Template Name: Portfolio
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header(); ?>
<?php $bannerImage = get_field('main_banner');?>
<div class="wrapper maincontent" id="full-width-page-wrapper">
	  <!-- Start Main Banner Section -->
    <section class="section main-banner-section" style="background-image: url('<?php echo $bannerImage['url']; ?>');">
        <div class="container-1600">
            <div class="col-12">
            	<div class="data">
            		<h1 class="title"><?php the_field('title'); ?></h1>
            		<p><?php the_field('subtitle'); ?></p>
            	</div>
            	<div class="scroll-down">
            		<a href="#"><span></span><p><?php the_field('scroll_down_text') ?></p></a>
            	</div>
            </div>
        </div>
    </section>
    <!-- End Main Banner Section -->

    <!-- Start of Layout Section -->
    <?php
	// check if the flexible content field has rows of data
	if( have_rows('layouts') ):
	     // loop through the rows of data
	    while ( have_rows('layouts') ) : the_row();

	        if( get_row_layout() == 'portrait_photos' ): ?>
	        <section class="layout-section portrait-photos">
	        	<div class="container-1600">
	        		<div class="row">
	        			<div class="col-md-6 padding-0">
	        				<?php $firstImage = get_sub_field('first_image'); ?>
	        				<img src="<?php echo $firstImage['url']; ?>" alt="">
	        			</div>
	        			<div class="col-md-6 padding-0">
	        				<?php $secondImage = get_sub_field('second_image'); ?>
	        				<img src="<?php echo $secondImage['url'] ?>" alt="">
	        			</div>
	        		</div>
	        	</div>
	        	 	
	        </section>

	        <?php elseif( get_row_layout() == 'landscape_photo' ): ?>
	        <section class="layout-section landscape-photo">
	        	<?php $image = get_sub_field('image') ?>
	        	<div class="container-1600">
	        		<div class="row">
	        			<div class="col-12 padding-0">
	        				<img src="<?php echo $image['url']; ?>" alt="">
	        			</div>
					</div>
	        	</div>
	        </section>


        <?php endif;
		    endwhile;

			else :

			    // no layouts found

			endif;
	?>
    <!-- End of Layout Section -->
</div>

<?php 
get_footer();