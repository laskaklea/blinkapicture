<?php
/**
 * Template Name: Paintings
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();  ?>
<div class="wrapper maincontent" id="full-width-page-wrapper">
	<section class="section section-paintings">
		<div class="container-1600">
			<div class="row">
				<div class="col-lg-12">
                    <div class="title">
                        <h1 class="title-h1 title-calcio">
                            <?php the_title(); ?>
                        </h1>
                    </div>
                </div>
			</div>
			<div class="row">
				<?php while(have_rows('paintings')) : the_row(); ?>
					<div class="col-md-6 col-lg-4">
						<?php $image = get_sub_field('image') ?>
						<div class="image-wrapper">
							<img src="<?php echo $image['url']; ?>" alt="">
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</section>
</div>
<?php 
get_footer();