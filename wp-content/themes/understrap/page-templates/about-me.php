<?php
/**
 * Template Name: About Me
 *
 * Template for displaying about me page
 * 
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );//?

?>

<div class="wrapper" id="page-wrapper">

    <div class="container-1600" id="content" tabindex="-1">
        <div class="row about-me-row">
            <!--start photo section-->
            <div class="col-lg-4">
                <section class="section photo-section">
                    <?php $image = get_field('about_me'); ?>
                    <?php //var_dump($image);  ?>
                    <div class="image-shadow" style="background-image : url('<?php echo $image['url']; ?>');"> </div>
                </section>
                <!--End photo section-->
            </div>

            <div class="col-lg-8">
                <div class="row">
                <section class="right-side-data">
                    <div class="row">
                        <!-- Start Top Section -->
                        <?php if( get_field('text')) : ?>
                        <div class="col-12">
                            <section class="section text-section">
                                <p><?php the_field('text') ?></p>
                            </section>
                        </div>
                        <?php endif; ?>
                        <!-- End Top Side Section -->

                        <?php if( get_field('description')) : ?>
                        <!-- Start Left Side Section -->
                        <div class="col-lg-6">
                            <section class="section desc-section">
                                <p><?php the_field('description') ?></p>
                            </section>
                        </div>
                        <!-- End Left Side Section -->
                        <?php endif; ?>

                        <?php if( get_field('description2')) : ?>
                        <!-- Start Right Side Section -->
                        <div class="col-lg-6">
                            <section class="section desc2-section">
                                <p><?php the_field('description2') ?></p>
                            </section>
                        </div>
                        <!-- End Right Side Section -->
                        <?php endif; ?>

                        <?php if( get_field('contact_me_text')) : ?>
                        <!-- Start Bottom Section -->
                        <div class="col-12">
                            <section class="section contact-section">
                                <p><?php the_field('contact_me_text') ?></p>
                            </section>
                        </div>
                        <!-- End Bottom Section -->
                        <?php endif; ?>
                    </div>
                </section>
                </div>
            </div>

        </div>
        <!-- .row -->

    </div>
    <!-- #content -->

</div>
<!-- #page-wrapper -->

<?php get_footer(); ?>