<?php 
if ( ! function_exists('cpt_testimonial') ) {

// Register Custom Post Type
function cpt_testimonial() {

	$labels = array(
		'name'                  => _x( 'Testimonials', 'Post Type General Name', 'sage' ),
		'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'sage' ),
		'menu_name'             => __( 'Testimonials', 'sage' ),
		'name_admin_bar'        => __( 'Testimonial', 'sage' ),
		'archives'              => __( 'Testimonial Archives', 'sage' ),
		'parent_item_colon'     => __( 'Parent Testimonial:', 'sage' ),
		'all_items'             => __( 'All Testimonials', 'sage' ),
		'add_new_item'          => __( 'Add New Testimonials', 'sage' ),
		'add_new'               => __( 'Add New', 'sage' ),
		'new_item'              => __( 'New Testimonial', 'sage' ),
		'edit_item'             => __( 'Edit Testimonial', 'sage' ),
		'update_item'           => __( 'Update Testimonial', 'sage' ),
		'view_item'             => __( 'View Testimonial', 'sage' ),
		'search_items'          => __( 'Search Testimonial', 'sage' ),
		'not_found'             => __( 'Not found', 'sage' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sage' ),
		'featured_image'        => __( 'Featured Image', 'sage' ),
		'set_featured_image'    => __( 'Set featured image', 'sage' ),
		'remove_featured_image' => __( 'Remove featured image', 'sage' ),
		'use_featured_image'    => __( 'Use as featured image', 'sage' ),
		'insert_into_item'      => __( 'Insert into Testimonial', 'sage' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Testimonial', 'sage' ),
		'items_list'            => __( 'Testimonials list', 'sage' ),
		'items_list_navigation' => __( 'Testimonials list navigation', 'sage' ),
		'filter_items_list'     => __( 'Filter Testimonials list', 'sage' ),
	);
	$args = array(
		'label'                 => __( 'Testimonial', 'sage' ),
		'description'           => __( 'Testimonials from your customers.', 'sage' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'excerpt', 'custom-fields'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-format-quote',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'testimonials', $args );

}
add_action( 'init', 'cpt_testimonial', 0 );

}